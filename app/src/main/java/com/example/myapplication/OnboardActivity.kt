package com.example.myapplication

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

const val TABLE="TABLE"//задаем контанту TABLE
class OnboardActivity : AppCompatActivity() {
    lateinit var tl: TabLayout
    lateinit var vp:ViewPager2
    //задаем переменные tl и vp для перехода между фрагментами
    var skip:SharedPreferences?=null//задаем экземпляр класса SharedPreferences skip для
    // сохранения информации о нажатии на кнопки "Пропустить", "Пропустить" и "Завершить"
    // и присваевам начальное значение null
    val falist= listOf(Onboard1Fragment(), Onboard2Fragment(), Onboard3Fragment())//задаем список фрагментов 
    // для дальнейшей работы с переменными tl и vp
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboard)
        skip=getSharedPreferences(TABLE, MODE_PRIVATE)//для инициализации переменной skip пишем таблицу TABLE,
        // куда будем вписывать значения и режим работы
        tl=findViewById(R.id.tab_layout)
        vp=findViewById(R.id.viewPager2)//задаем id для переменных tl и vp
        val adapter=viewadapter(this, falist)//задаем константу adapter и присваеваем ее к классу viewadapter
        vp.adapter=adapter//присваеваем
        TabLayoutMediator(tl, vp) { tab, position ->

        }.attach()//реализуем

    }
    fun savestate(i:Int){//функция для задания значения i с ключом button_state
        val editor=skip?.edit()
        editor?.putInt("button_state", i)
        editor?.apply()
    }

    fun miss1(view: View) {
        val intent=Intent(this@OnboardActivity, RegActivity::class.java)
        startActivity(intent)
        savestate(1)
    }
    fun miss2(view: View) {
        val intent=Intent(this@OnboardActivity, RegActivity::class.java)
        startActivity(intent)
        savestate(1)
    }
    fun complete(view: View) {
        val intent=Intent(this@OnboardActivity, RegActivity::class.java)
        startActivity(intent)
        savestate(1)
    }
}