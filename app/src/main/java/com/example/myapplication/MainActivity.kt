package com.example.myapplication

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.WindowManager

class MainActivity : AppCompatActivity() {
    var skip:SharedPreferences?=null
    var state=0
    var checT:Boolean=false//эти 3 переменные нужны для того чтобы опредлить на какие экраны мы будем переключаться/пропускать
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)//пишем,
        // что наше активити будет занимать польностью весь экран, включая статус бар
        setContentView(R.layout.activity_main)
        skip=getSharedPreferences(TABLE, MODE_PRIVATE)
        state=skip?.getInt("button_state", 0)!!//узнаем из skip значение button_state
        val timer = object : CountDownTimer(3000, 1000){//задаем переменную timer для перехода на другой экран
            //на определенное время
            override fun onTick(millisUntilFinished: Long) {
                checT=state==1//условие, по которому узнаем значение переменной checT
            }

            override fun onFinish() {
                if(checT){//условие для перехода/пропуска экранов
                    val intent=Intent(this@MainActivity, RegActivity::class.java)//определяем переменную intent
                    //для перехода из MainActivity в RegActivity, при условии, что checT равен true
                    startActivity(intent)//начинаем действие перехода
                }else{
                    val intent = Intent(this@MainActivity, OnboardActivity::class.java)//определяем переменную intent
                    //для перехода из MainActivity в  OnboardActivity, при условии, что checT равен false
                    startActivity(intent)//начинаем действие перехода
                }

            }

        }
        timer.start()//начать timer
    }
}